// module.exports = {
//   devServer: {
//     disableHostCheck: true
//   }
// }

module.exports = {
  css: {
    sourceMap: true
  },
  devServer: {
    watchOptions: {
      poll: true,
      disableHostCheck: true
    }
  }
}
