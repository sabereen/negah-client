import Vue from 'vue'
import Router from 'vue-router'
import Login from './views/Login.vue'
import ObjectList from './views/ObjectList.vue'
import Receipt from './views/Receipt.vue'
import History from './views/History.vue'
import Profile from './views/Profile.vue'
import NoItem from './components/NoItem.vue'

Vue.use(Router)

var router = new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/login',
      name: 'login',
      component: Login
    },
    {
      path: '/',
      name: 'movies',
      component: ObjectList
    },
    {
      path: '/bot',
      name: 'bot',
      component: ObjectList
    },
    {
      path: '/receipt',
      name: 'receipt',
      component: Receipt
    },
    {
      path: '/history',
      name: 'history',
      component: History
    },
    {
      path: '/profile',
      name: 'profile',
      component: Profile
    },
    {
      path: '*',
      name: '404',
      component: NoItem,
      props: { text: '۴۰۴ - این صفحه وجود ندارد' }
    }
    // {
    //   path: '/',
    //   name: 'home',
    //   component: Home
    // },
    // {
    //   path: '/about',
    //   name: 'about',
    //   // route level code-splitting
    //   // this generates a separate chunk (about.[hash].js) for this route
    //   // which is lazy-loaded when the route is visited.
    //   component: () => import(/* webpackChunkName: "about" */ './views/ObjectList.vue')
    // }
  ]
})

router.beforeEach((to, from, next) => {
  if (to.name !== 'login' && to.name !== 'bot' && !window.localStorage.getItem('studentId')) {
    next({ path: '/login' })
  } else {
    next()
  }
})

export default router
