import './polyfill'
import 'nprogress'
import Vue from 'vue'
import VueResource from 'vue-resource'
import App from './App.vue'
import router from './router'

Vue.config.productionTip = false

Vue.use(VueResource)

new Vue({
  router,
  data: {
    role: 'user',
    token: window.localStorage.getItem('token'),
    studentId: window.localStorage.getItem('studentId'),
    apiRoot: Vue.config.devtools ? 'http://localhost:6672/api' : '/api'
  },
  computed: {
    isAdmin () {
      return this.role === 'admin'
    }
  },
  http: {
    // root: 'http://localhost:6672/api/',
    headers: {
      Authorization: 'Bearer token'
    }
  },
  render: h => h(App)
}).$mount('#app')

Vue.directive('editable', {
  inserted (el) {
    el.oninput = function (event) {
      console.log('input', event)
    }
  }
})
